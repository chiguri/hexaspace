package info.chiguri.hexaspace;

import java.awt.Graphics;

public interface HexaEntity {
	public int angle(HexaEntity e);
	public String id();
	public void draw(Graphics g, int x, int y, int size);
}
