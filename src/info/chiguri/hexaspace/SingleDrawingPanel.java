package info.chiguri.hexaspace;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class SingleDrawingPanel extends JPanel implements AddDrawingInterface {
	HexaDrawing draw;

	public SingleDrawingPanel(HexaDrawing draw) {
		super();
		this.draw = draw;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		final int size = 100;
		final int arrowsize = 40;
		final int strmargin = 10;
		final float fontsize = 10;
		final int margin = 50;
		final int topmargin = size + strmargin + (int)fontsize + strmargin + margin;

		if(getWidth() <= margin*2 || getHeight() <= topmargin + margin || draw == null) {
			return; // do not draw anything
		}
		HexaSpacePoint[] ps = draw.drawingPoints();

		double min_x, min_y, max_x, max_y;
		min_x = min_y = 0.0;
		max_x = max_y = 1.0;
		for(HexaSpacePoint p : ps) {
			if(p.orth_x < min_x) min_x = p.orth_x;
			if(p.orth_x > max_x) max_x = p.orth_x;
			if(p.orth_y < min_y) min_y = p.orth_y;
			if(p.orth_y > max_y) max_y = p.orth_y;
		}

		double d_width = max_x - min_x;
		double d_height = max_y - min_y;
		double w_ratio = (double)(getHeight()-topmargin-margin) / (double)(getWidth()-margin*2);
		double ratio;
		if(d_height/d_width < w_ratio) {
			ratio = (double)(getWidth()-margin*2) / d_width;
		}
		else {
			ratio = (double)(getHeight()-topmargin-margin) / d_height;
		}

		int[] xs = new int[ps.length];
		int[] ys = new int[ps.length];
		for(int i = 0; i < ps.length; ++i) {
			xs[i] = (int)((ps[i].orth_x-min_x) * ratio) + margin;
			ys[i] = (int)((ps[i].orth_y-min_y) * ratio) + topmargin;
		}
		for(int i = 1; i < ps.length; ++i) {
			HexaArrow.drawLine(g, xs[i-1], ys[i-1], xs[i], ys[i], (int)(ratio/20));
		}

		g.setFont(getFont().deriveFont(fontsize));

		for(int i = 0; i < draw.entities.length; ++i) {
			g.drawString(draw.entities[i].id(), i*arrowsize+strmargin+10, arrowsize+strmargin+10);
			g.drawString(Integer.toString(draw.arrowlengths[i]), i*arrowsize+strmargin+10, arrowsize+strmargin+20);
			draw.entities[i].draw(g, i*arrowsize+10, 10, arrowsize);
		}

		for(int i = 1; i < ps.length; ++i) {
			if(xs[i-1] != xs[i] || ys[i-1] != ys[i]) {
				HexaArrow.drawLine(g, xs[i-1], ys[i-1], xs[i], ys[i], 10);
			}
			else {
				HexaPoint.drawPoint(g, xs[i], ys[i]);
			}
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		SingleDrawingPanel panel = new SingleDrawingPanel(HexaDrawing.parseDrawing("012345:1 2 2 1 2 2"));
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		DrawingSetFrame.showFrame(frame, panel);
	}

	@Override
	public void addDrawing(HexaDrawing draw) {
		this.draw = draw;
	}

	@Override
	public void setDrawing(HexaDrawing draw) {
		this.draw = draw;
	}
}
