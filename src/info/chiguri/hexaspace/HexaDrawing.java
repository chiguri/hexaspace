package info.chiguri.hexaspace;

public class HexaDrawing {

	public final HexaEntity[] entities;
	protected int[] arrowlengths;

	private void arrowCheck() {
		for(int i = 0; i < entities.length; ++i) {
			if(entities[i] == null) {
				throw new RuntimeException("Non-expected arrow:" + entities[i]);
			}
		}
	}

	public HexaDrawing(HexaEntity[] entities) {
		this.entities = entities;
		arrowCheck();
	}

	public HexaDrawing(HexaEntity[] entities, int[] arrowlengths) {
		this.entities = entities;
		arrowCheck();
		setLength(arrowlengths);
	}

	void setLength(int[] arrowlengths) {
		if(entities.length != arrowlengths.length) {
			throw new RuntimeException("Number of arrows are not the same as lengths");
		}
		this.arrowlengths = arrowlengths;
	}

	@Override
	public String toString() {
		if(arrowlengths != null)
			return entityString() + ":" + lengthString();
		else
			return entityString();
	}

	public String entityString() {
		String str = "";
		for(HexaEntity a : entities) {
			str += a.id() + " ";
		}
		return str;
	}

	public String lengthString() {
		String str = "";
		for(int l : arrowlengths) {
			str += l + " ";
		}
		return str;
	}


	public static HexaEntity[] parseEntities(String str) {
		str = str.replaceAll("[ \t\r\n]+", "");
		HexaEntity[] entities = new HexaEntity[str.length()];
		for(int i = 0; i < str.length(); ++i) {
			char id = str.charAt(i);
			if(Character.isDigit(id)) {
				entities[i] = HexaArrow.numberedArrow(Integer.parseInt(str.substring(i, i+1)));
			}
			else {
				entities[i] = new HexaPoint(id);
			}
		}
		return entities;
	}

	public static int[] parseLengths(String str) {
		String[] strs = str.split("[ \t\r\n]+");
		if(strs[0].equals("")) {
			int[] lengths = new int[strs.length-1];
			for(int i = 1; i < strs.length; ++i) {
				lengths[i-1] = Integer.parseInt(strs[i]);
			}
			return lengths;
		}
		else {
			int[] lengths = new int[strs.length];
			for(int i = 0; i < strs.length; ++i) {
				lengths[i] = Integer.parseInt(strs[i]);
			}
			return lengths;
		}
	}

	public static HexaDrawing parseDrawing(String str) {
		String[] strs = str.split(":");
		HexaEntity[] entities = parseEntities(strs[0]);
		if(strs.length != 2) {
			return new HexaDrawing(entities);
		}
		int[] arrowlengths = parseLengths(strs[1]);
		return new HexaDrawing(entities, arrowlengths);
	}

	public HexaSpacePoint[] drawingPoints() {
		if(arrowlengths == null) {
			throw new RuntimeException("Lengths are not set -- " + toString());
		}
		HexaSpacePoint[] points = new HexaSpacePoint[entities.length+1]; // +1 : for start point
		points[0] = new HexaSpacePoint(0, 0);
		for(int i = 0; i < entities.length; ++i) {
			int x = points[i].x;
			int y = points[i].y;
			points[i+1] = new HexaSpacePoint(x+HexaSpacePoint.dx(entities[i])*arrowlengths[i], y+HexaSpacePoint.dy(entities[i])*arrowlengths[i]);
		}
		return points;
	}
}
