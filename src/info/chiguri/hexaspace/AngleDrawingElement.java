package info.chiguri.hexaspace;

class AngleDrawingElement {
	final String drawing;
	final String elem;
	AngleDrawingElement(String drawing, String elem) {
		this.drawing = drawing;
		this.elem = elem;
	}

	public String toString() {
		return elem;
	}

	public static int[] parseElement(String str) {
		str = str.replaceFirst("^\\s*", "");
		String[] res = str.split("\\s+"); // 手抜き（空白区切りすること）
		if(res.length == 0 || res[0].equals("")) {
			return new int[0];
		}
		int[] results = new int[res.length];
		System.out.println(res.length);
		for(int i = 0; i < res.length; ++i) {
			switch(res[i]) {
			case "A":
				results[i] = 2;
				break;
			case "B":
				results[i] = 1;
				break;
			case "-A":
				results[i] = -2;
				break;
			case "-B":
				results[i] = -1;
				break;
			default:
				throw new RuntimeException("Cannot Parse Input : " + str);
			}
		}
		return results;
	}

	public static String toElement(int[] input) {
		if(input.length == 0 || input[0] == 0) return "";
		String str = toElement(input[0]);
		for(int i = 1; input[i] != 0; ++i) {
			str += " " + toElement(input[i]);
		}
		return str;
	}

	private static String toElement(int n) {
		if(n == 2) return "A";
		if(n == 1) return "B";
		if(n == -1) return "-B";
		if(n == -2) return "-A";

		throw new RuntimeException("Not element number : " + n);
	}
}