package info.chiguri.hexaspace;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;

public class Constraints {
	public static void checkConstraints(HexaExpression exp) {
		checkLineOverlap(exp.entities, exp.closed);
		checkPointOverlap(exp.entities, exp.closed);
		checkPointConstraints(exp.entities, exp.closed);
		checkClosedConstraints(exp.entities, exp.closed);
	}

	private static void error(String message) {
		throw new RuntimeException(message);
	}

	private static void checkExistence(HashMap<HexaPoint, HashSet<HexaArrow>> inout, HexaPoint p, HexaArrow a) {
		HashSet<HexaArrow> hs;
		if(!inout.containsKey(p)) {
			hs = new HashSet<>();
			hs.add(a);
			inout.put(p, hs);
			return;
		}

		hs = inout.get(p);
		if(hs.contains(a)) {
			throw new RuntimeException("Point " + p.id() + " has two lines whose type is " + a.id());
		}
		hs.add(a);
	}

	// precondition(redundancy): lines should not appear in succession
	// 2: line and its reverse line should not appear in succession
	private static void checkLineOverlap(HexaEntity[]entities, boolean closed) {
		for(int i = 1; i < entities.length; ++i) {
			if(entities[i-1] instanceof HexaArrow && entities[i] instanceof HexaArrow && ((HexaArrow)entities[i-1]).id%3 == ((HexaArrow)entities[i]).id%3) {
				error("Lines (or one's reverse) are successive : " + entities[i-1].id() + " and " + entities[i].id() + " at " + (i-1) + "-" + i);
			}
		}
		if(closed) {
			if(entities[entities.length-1] instanceof HexaArrow && entities[0] instanceof HexaArrow && ((HexaArrow)entities[entities.length-1]).id%3 == ((HexaArrow)entities[0]).id%3) {
				error("Lines (or one's reverse) are successive : " + entities[entities.length-1].id() + " and " + entities[0].id() + " at " + (entities.length-1) + "-" + 0);
			}
		}
	}

	// 1: points should not appear in succession
	private static void checkPointOverlap(HexaEntity[] entities, boolean closed) {
		for(int i = 1; i < entities.length; ++i) {
			if(entities[i-1] instanceof HexaPoint && entities[i] instanceof HexaPoint) {
				error("Points are successive : " + entities[i-1].id() + " and " + entities[i].id() + " at " + (i-1) + "-" + i);
			}
		}
		if(closed) {
			if(entities[entities.length-1] instanceof HexaPoint && entities[0] instanceof HexaPoint) {
				error("Points are successive : " + entities[entities.length-1].id() + " and " + entities[0].id() + " at " + (entities.length-1) + "-" + entities.length);
			}
		}
	}

	// 3:
	private static void checkPointConstraints(HexaEntity[] entities, boolean closed) {
		HashMap<HexaPoint, HashSet<HexaArrow>> inout = new HashMap<>();
		if(closed) {
			if(entities[0] instanceof HexaPoint) {
				checkExistence(inout, (HexaPoint)entities[0], (HexaArrow)entities[entities.length-1]);
			}
			if(entities[entities.length-1] instanceof HexaPoint) {
				checkExistence(inout, (HexaPoint)entities[entities.length-1], ((HexaArrow)entities[0]).reverse());
			}
		}
		for(int i = 1; i < entities.length; ++i) {
			if(entities[i-1] instanceof HexaPoint) {
				checkExistence(inout, (HexaPoint)entities[i-1], ((HexaArrow)entities[i]).reverse());
			}
			if(entities[i] instanceof HexaPoint) {
				checkExistence(inout, (HexaPoint)entities[i], (HexaArrow)entities[i-1]);
			}
		}
	}

	// 6: split sequence by tangent point should be 2pi rotate
	private static void checkClosedConstraints(HexaEntity[] entities, boolean closed) {
		if(closed) {
			for(int i = 0; i < entities.length; ++i) {
				if(entities[i] instanceof HexaPoint) {
					HexaPoint p1 = (HexaPoint)entities[i];
					for(int j = 1; j < entities.length; ++j) {
						if(entities[(i+j)%entities.length].equals(p1)) {
							int angles = 0;
							for(int k = i+2; k < i+j; ++k) {
								if(entities[k%entities.length] instanceof HexaArrow) {
									angles += entities[(k-1)%entities.length].angle(entities[k%entities.length]);
								}
								else {
									angles += entities[(k-1)%entities.length].angle(entities[(k+1)%entities.length]);
								}
							}
							angles += entities[(i+j-1)%entities.length].angle(entities[(i+1)%entities.length]);
							if(angles != 6 && angles != -6) {
								throw new RuntimeException("Tangent part is not rotate : " + i + "-" + ((i+j)%entities.length) + " but " + angles);
							}
						}
					}
				}
			}
		}
		else {
			for(int i = 0; i < entities.length; ++i) {
				if(entities[i] instanceof HexaPoint) {
					HexaPoint p1 = (HexaPoint)entities[i];
					for(int j = i+1; j < entities.length; ++j) {
						if(entities[j].equals(p1)) {
							int angles = 0;
							for(int k = i+2; k < j; ++k) {
								if(entities[k] instanceof HexaArrow) {
									angles += entities[k-1].angle(entities[k]);
								}
								else {
									angles += entities[k-1].angle(entities[k+1]);
								}
							}
							angles += entities[j-1].angle(entities[i+1]);
							if(angles != 6 && angles != -6) {
								throw new RuntimeException("Tangent part is not rotate : " + i + "-" + j);
							}
						}
					}
				}
			}
		}
	}

	private static boolean rotateSumCheck(int[] arrows) {
		int angles = 0;
		for(int j = 0; j < arrows.length; ++j) {
			angles += (arrows[(j+1)%arrows.length] - arrows[j] + 8) % 6 - 2;
		}
		return (angles == 6 || angles == -6);
	}

	// successive (reverse) line is not allowed
	// rotate sum should be 2pi
	// any other? how about point?
	private static void nextTestcase(int[] arrows) {
		++arrows[arrows.length-1];
		int i = arrows.length-1;
		while(i < arrows.length) {
			while(i > 0) {
				if(arrows[i] >= 6) {
					arrows[i] = 0;
					++arrows[--i];
				}
				else {
					break;
				}
			}
			if(arrows[0] >= 6) return;

			for(i = 1; i < arrows.length; ++i) {
				if(arrows[i-1]%3 == arrows[i]%3) {
					++arrows[i];
					break;
				}
			}
			if(i == arrows.length) {
				if(arrows[0]%3 != arrows[arrows.length-1]%3) {
					if(rotateSumCheck(arrows)) return;
				}
				++arrows[--i];
			}
		}
	}

	public static void generateTests(int length, PrintStream out) {
		if(length < 2) {
			return;
		}
		int[] arrows = new int[length];
		for(int i = 0; i < length; ++i) {
			arrows[i] = 0;
		}

		nextTestcase(arrows);
		while(arrows[0] < 6) {
			out.print("[");
			for(int a : arrows) {
				out.print(a);
			}
			out.println("]");
			nextTestcase(arrows);
		}
	}

	public static void main(String[] args) {
		//generateTests(10, System.out);
		try {
			generateTests(10, new PrintStream(new FileOutputStream("testcase.txt")));
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
