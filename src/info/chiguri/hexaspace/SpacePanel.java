package info.chiguri.hexaspace;

import java.awt.Graphics;
import java.util.LinkedList;

import javax.swing.JPanel;

public class SpacePanel extends JPanel implements AddDrawingInterface {
	static final int margin = 50;

	LinkedList<HexaDrawing> draws;

	public SpacePanel() {
		super();
		draws = new LinkedList<>();
	}

	@Override
	public void addDrawing(HexaDrawing draw) {
		draws.add(draw);
	}

	@Override
	public void setDrawing(HexaDrawing draw) {
		draws.clear();
		draws.add(draw);
	}

	@Override
	public void paintComponent(Graphics g) {
		if(getWidth() <= margin*2 || getHeight() <= margin*2) {
			return; // do not draw anything
		}
		HexaSpacePoint[][] drawers = new HexaSpacePoint[draws.size()][];
		{
			int i = 0;
			for(HexaDrawing d : draws) {
				drawers[i++] = d.drawingPoints();
			}
		}
		double min_x, min_y, max_x, max_y;
		min_x = min_y = 0.0;
		max_x = max_y = 1.0;
		for(HexaSpacePoint[] ps : drawers) {
			for(HexaSpacePoint p : ps) {
				if(p.orth_x < min_x) min_x = p.orth_x;
				if(p.orth_x > max_x) max_x = p.orth_x;
				if(p.orth_y < min_y) min_y = p.orth_y;
				if(p.orth_y > max_y) max_y = p.orth_y;
			}
		}

		double d_width = max_x - min_x;
		double d_height = max_y - min_y;
		double w_ratio = (double)(getHeight()-margin*2) / (double)(getWidth()-margin*2);
		double ratio;
		if(d_height/d_width < w_ratio) {
			ratio = (double)(getWidth()-margin*2) / d_width;
		}
		else {
			ratio = (double)(getHeight()-margin*2) / d_height;
		}

		for(HexaSpacePoint[] ps : drawers) {
			int[] xs = new int[ps.length];
			int[] ys = new int[ps.length];
			for(int i = 0; i < ps.length; ++i) {
				xs[i] = (int)((ps[i].orth_x-min_x) * ratio) + margin;
				ys[i] = (int)((ps[i].orth_y-min_y) * ratio) + margin;
			}
			for(int i = 1; i < ps.length; ++i) {
				HexaArrow.drawLine(g, xs[i-1], ys[i-1], xs[i], ys[i], (int)(ratio/20));
			}
		}
	}
}
