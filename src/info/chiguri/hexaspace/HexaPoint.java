package info.chiguri.hexaspace;

import java.awt.Graphics;

public class HexaPoint implements HexaEntity {
	public static final int RADIUS = 5;
	public final char id;
	public final String id_str;

	public static void drawPoint(Graphics g, int x, int y) {
		g.fillOval(x - RADIUS, y - RADIUS, RADIUS * 2, RADIUS * 2);
	}

	public HexaPoint(char id) {
		this.id = id;
		this.id_str = Character.toString(id);
	}

	@Override
	public int angle(HexaEntity e) {
		return 0;
	}

	@Override
	public String id() {
		return id_str;
	}

	@Override
	public void draw(Graphics g, int x, int y, int size) {
		size /= 2;
		drawPoint(g, x + size, y + size);
	}

	@Override
	public boolean equals(Object o) {
		return ((o instanceof HexaPoint) && ((HexaPoint)o).id == id);
	}

	@Override
	public int hashCode() {
		return Character.hashCode(id);
	}
}
