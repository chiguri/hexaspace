package info.chiguri.hexaspace;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public interface DrawingGenerator {
	public HexaDrawing exp2draw(HexaExpression exp);
	default public void expfile2drawfile(String expfilename, String drawfilename) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(expfilename));
			PrintStream out = new PrintStream(new FileOutputStream(drawfilename));

			while(true) {
				String line = reader.readLine();
				if(line == null) break;
				HexaDrawing draw = exp2draw(HexaExpression.parseExpression(line));
				if(draw == null) {
					reader.close();
					out.close();
					throw new RuntimeException(line + " cannot be drawn by this method");
				}
				out.println(draw);
			}
			reader.close();
			out.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
}
