package info.chiguri.hexaspace;

import java.awt.Graphics;

public enum HexaArrow implements HexaEntity {
	RIGHT(0),
	RIGHTTOP(1),
	LEFTTOP(2),
	LEFT(3),
	LEFTBOTTOM(4),
	RIGHTBOTTOM(5);

	static private final HexaArrow[] arrows = { RIGHT, RIGHTTOP, LEFTTOP, LEFT, LEFTBOTTOM, RIGHTBOTTOM };

	static private final double HALF_ROOT3 = Math.sqrt(3.0)/2;
	final int id;
	final String id_str;

	private HexaArrow(int id) {
		this.id = id;
		this.id_str = Integer.toString(id);
	}

	public static int number(HexaArrow arrow) {
		if(arrow == null) {
			throw new RuntimeException("Not arrow is given (null)");
		}
		return arrow.id;
	}

	public static HexaArrow numberedArrow(int arrow) {
		if(0 <= arrow && arrow < 6) {
			return arrows[arrow];
		}
		throw new RuntimeException("Unexpected number is given: " + arrow);
	}

	public static void drawLine(Graphics g, int x1, int y1, int x2, int y2, int arrowsize) {
		g.drawLine(x1, y1, x2, y2);
		int[] tri_x = new int[4];
		int[] tri_y = new int[4];
		tri_x[0] = tri_x[3] = x2;
		tri_y[0] = tri_y[3] = y2;
		double length = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
		double sine = (y1 - y2) / length;
		double cosine = (x1 - x2) / length;
		tri_x[1] = x2 + (int)((cosine * HALF_ROOT3 - sine * 0.5) * arrowsize);
		tri_x[2] = x2 + (int)((cosine * HALF_ROOT3 + sine * 0.5) * arrowsize);
		tri_y[1] = y2 + (int)((sine * HALF_ROOT3 + cosine * 0.5) * arrowsize);
		tri_y[2] = y2 + (int)((sine * HALF_ROOT3 - cosine * 0.5) * arrowsize);
		g.fillPolygon(tri_x, tri_y, 4);
	}

	public static void drawArrow(Graphics g, HexaArrow arrow, int x, int y, int size) {
		size = ((size*4)/5);
		int arrowsize = Math.max(Math.min(size/10, 10), 5);
		int left = x;
		int right = x+size;
		int top = y;
		int bottom = y+size;
		int halfsize = size/2;
		int center_x = x+halfsize;
		int center_y = y+halfsize;
		int slew = (int)(size/HALF_ROOT3/4);
		int center_left = center_x-slew;
		int center_right = center_x+slew;
		switch(arrow) {
		case RIGHT:
			drawLine(g, left, center_y, right, center_y, arrowsize);
			break;

		case RIGHTTOP:
			drawLine(g, center_left, bottom, center_right, top, arrowsize);
			break;

		case LEFTTOP:
			drawLine(g, center_right, bottom, center_left, top, arrowsize);
			break;

		case LEFT:
			drawLine(g, right, center_y, left, center_y, arrowsize);
			break;

		case LEFTBOTTOM:
			drawLine(g, center_right, top, center_left, bottom, arrowsize);
			break;

		case RIGHTBOTTOM:
			drawLine(g, center_left, top, center_right, bottom, arrowsize);
			break;

		default:
			throw new RuntimeException("Not arrow is given (maybe null?)");
		}
	}

	public static int rotateAngle(HexaArrow arrow1, HexaArrow arrow2) {
		return (arrow2.id - arrow1.id + 8) % 6 - 2;
	}

	public int number() {
		return id;
	}

	public HexaArrow reverse() {
		return arrows[(id+3)%6];
	}

	@Override
	public String id() {
		return id_str;
	}

	@Override
	public void draw(Graphics g, int x, int y, int size) {
		drawArrow(g, this, x, y, size);
	}

	@Override
	public int angle(HexaEntity e) {
		if(e instanceof HexaArrow) {
			return (((HexaArrow)e).id - this.id + 8) % 6 - 2;
		}
		return 0;
	}
}
