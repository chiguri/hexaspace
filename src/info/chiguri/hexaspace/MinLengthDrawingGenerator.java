package info.chiguri.hexaspace;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;

public class MinLengthDrawingGenerator implements DrawingGenerator {
	protected int max;
	protected int length;
	protected HexaExpression exp;
	protected int[] lengths;
	protected int[][] field;
	protected int[] dx;
	protected int[] dy;
	protected int init_x;
	protected int init_y;
	protected int last;
	protected HashMap<HexaPoint, Integer> px;
	protected HashMap<HexaPoint, Integer> py;

	private void init(HexaExpression exp) {
		this.exp = exp;
		lengths = new int[exp.entities.length];
		length = 0;
		for(int i = 0; i < exp.entities.length; ++i) {
			if(exp.entities[i] instanceof HexaArrow) {
				++length;
				//lengths[i] = 1;
				last = i;
			}
			else {
				//lengths[i] = 0;
			}
		}

		// TODO how do we estimate plenty length to draw?
		max =length * 5;
		field = new int[lengths.length*4+2][lengths.length*4+2];
		for(int i = 0; i < field.length; ++i) {
			field[0][i] = field[field.length-1][i] = field[i][0] = field[i][field.length-1] = -1;
		}
		dx = new int[exp.entities.length];
		dy = new int[exp.entities.length];
		for(int i = 0; i < exp.entities.length; ++i) {
			dx[i] = HexaSpacePoint.dx(exp.entities[i]);
			dy[i] = HexaSpacePoint.dy(exp.entities[i]);
		}
		init_x = init_y = field.length/2;

		px = new HashMap<>();
		py = new HashMap<>();
	}

	private boolean generate() {
		if(exp.closed && exp.entities[exp.entities.length-1] instanceof HexaPoint) {
			HexaPoint p = (HexaPoint)(exp.entities[exp.entities.length-1]);
			px.put(p, init_x);
			py.put(p, init_y);
		}
		for(int rest = 0; rest < max; ++rest) {
			if(generate(0, rest, init_x, init_y)) {
				return true;
			}
		}
		return false;
	}

	private boolean lastCheck(int rest, int x, int y) {
		final int dx = this.dx[last];
		final int dy = this.dy[last];
		for(int i = 0; i <= rest; ++i) {
			if(field[x][y] != 0) return false;
			x += dx;
			y += dy;
		}
		lengths[last] = rest+1;
		if(exp.closed) {
			return (x == init_x && y == init_y);
		}
		if(exp.entities[exp.entities.length-1] instanceof HexaPoint) {
			HexaPoint p = (HexaPoint)(exp.entities[exp.entities.length-1]);
			if(px.containsKey(p) && (x != px.get(p) || y != py.get(p))) {
				return false;
			}
			field[x][y] = 0;
		}
		return field[x][y] == 0;
	}

	private boolean generate(int num, int rest, int x, int y) {
		if(num == last) {
			return lastCheck(rest, x, y);
		}
		if(exp.entities[num] instanceof HexaPoint) {
			HexaPoint p = (HexaPoint)(exp.entities[num]);
			if(px.containsKey(p)) {
				if(x == px.get(p) && y == py.get(p)) {
					int f = field[x][y];
					field[x][y] = 0;
					if(generate(num+1, rest, x, y)) {
						return true;
					}
					field[x][y] = f;
				}
				return false;
			}
			px.put(p, x);
			py.put(p, y);
			if(generate(num+1, rest, x, y)) {
				return true;
			}
			// revert point checking
			px.remove(p);
			py.remove(p);
			return false;
		}

		final int dx = this.dx[num];
		final int dy = this.dy[num];

		int i;
		for(i = 0; i <= rest; ++i) {
			if(field[x][y] != 0) {
				break;
			}
			field[x][y] = num+1;
			lengths[num] = i+1;
			x += dx;
			y += dy;
			if(generate(num+1, rest-i, x, y)) {
				return true;
			}
		}
		for( ; i > 0; --i) {
			x -= dx;
			y -= dy;
			field[x][y] = 0;
		}
		return false;
	}

	@Override
	public HexaDrawing exp2draw(HexaExpression exp) {
		if(exp == null) return null;

		init(exp);

		if(generate()) {
			return new HexaDrawing(exp.entities, lengths);
		}
		return null; // cannot generate: runtime exception?
	}

	public static void main(String[] args) {
		//HexaExpression exp = HexaExpression.parseExpression("[012402]");
		//System.out.println(new MinLengthDrawingGenerator().exp2draw(exp));
		//*
		try {
			System.out.println("TestCase generation");
			long testcase = System.currentTimeMillis();
			Constraints.generateTests(7, new PrintStream(new FileOutputStream("testcase.txt")));
			long drawing = System.currentTimeMillis();
			System.out.println("TestCase generation finished : " + (drawing-testcase) + "ms");
			System.out.println("Drawings generation");
			new MinLengthDrawingGenerator().expfile2drawfile("testcase.txt", "drawersfile.txt");
			System.out.println("Drawings generation finished : " + (System.currentTimeMillis()-drawing) + "ms");
			System.out.println("Show Drawings");
			DrawerFileViewer.view("drawersfile.txt");
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		// */
	}
}
