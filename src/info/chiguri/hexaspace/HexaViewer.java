package info.chiguri.hexaspace;

import javax.swing.JFrame;

public class HexaViewer {
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		SpacePanel panel = new SpacePanel();
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		DrawingSetFrame.showFrame(frame, panel);
	}
}
