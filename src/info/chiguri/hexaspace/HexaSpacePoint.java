package info.chiguri.hexaspace;

public class HexaSpacePoint {
	/**
	 * This point is on two dimensional plane with non-orthogonal axes.
	 * Horizontal axis (right) is as usual, but vertical axis (left-down) is tilted 30 degree
	 */
	final int x, y;
	final double orth_x, orth_y;
	public HexaSpacePoint(int x, int y) {
		this.x = x; this.y = y;
		orth_x = x - (double)y/2;
		orth_y = y*Math.sqrt(3)/2;
	}

	public static int dx(HexaEntity e) {
		if(e instanceof HexaArrow) {
			HexaArrow arrow = (HexaArrow)e;
			switch(arrow) {
			case RIGHT:
			case RIGHTBOTTOM:
				return 1;
			case LEFT:
			case LEFTTOP:
				return -1;
			case LEFTBOTTOM:
			case RIGHTTOP:
				return 0;
			default:
				throw new RuntimeException("Maybe null arrow is given");
			}
		}
		return 0;
	}

	public static int dy(HexaEntity e) {
		if(e instanceof HexaArrow) {
			HexaArrow arrow = (HexaArrow)e;
			switch(arrow) {
			case LEFTBOTTOM:
			case RIGHTBOTTOM:
				return 1;
			case LEFTTOP:
			case RIGHTTOP:
				return -1;
			case RIGHT:
			case LEFT:
				return 0;
			default:
				throw new RuntimeException("Maybe null arrow is given");
			}
		}
		return 0;
	}
}
