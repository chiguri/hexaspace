package info.chiguri.hexaspace;

// just translate C program into Java (with little modification)
public class NaiveSmoothing {
	static final int MAX_LEN = 30;
	final int[][] length;
	final int[][] figs;
	final int[] fig;

	public NaiveSmoothing(int[] fig) {
		if(fig.length > MAX_LEN) {
			throw new RuntimeException("Figure is too long to draw in the naive-smoothing method : max is " + MAX_LEN + " but " + fig.length);
		}
		this.fig = fig;

		figs = new int[MAX_LEN][MAX_LEN];
		length = new int[MAX_LEN][MAX_LEN];

		System.err.println(rotate(fig) +  "-time rotating");
		System.err.print("Input expression is :");
		for(int i = 0; i < fig.length; ++i) {
			System.err.print(" " + fig[i]);
		}
		System.err.println();
	}

	protected int rewrite_part(int num) {
		int i = 0;
		while(figs[num][i] > 0) ++i;
		if(figs[num][i] == 0) return -1; // no rewriting parts anymore -- how does it check if angle 0 is defined?
		++i;
		while(figs[num][i] < 0) ++i;
		if(figs[num][i] == 0) return -1; // tail: this should not occur (avoid by rotation)
		return i-1;
	}


	protected void smoothing(int num, int at) {
		for(int i = 0; i < at; ++i) {
			figs[num+1][i] = figs[num][i];
		}
		int t = figs[num][at]+figs[num][at+1];
		if(t == 0) {
			for(int i = at+2; figs[num][i] != 0; ++i) {
				figs[num+1][i-2] = figs[num][i];
			}
		}
		else {
			figs[num+1][at] = t;
			for(int i = at+2; figs[num][i] != 0; ++i) {
				figs[num+1][i-1] = figs[num][i];
			}
		}
	}


	static int rotate(int[] fig) {
		int sum = 0;
		final int len = fig.length;
		for(int i = 0; i < len; ++i) {
			sum += fig[i];
		}
		if(sum != 6) {
			throw new RuntimeException("This is not consistent expression");
		}

		int count = 0;
		while(true) {
			sum = fig[len-1];
			for(int i =len-2; i > 0 && sum > 0; --i) {
				sum += fig[i];
			}
			if(sum <= 0) {
				++count;
				int tail = fig[0];
				for(int i = 1; i < len; ++i) {
					fig[i-1] = fig[i];
				}
				fig[len-1] = tail;
			}
			else return count;
		}
	}


	protected void basic_fig(int num) {
		int len = 0;
		while(figs[num][len] != 0) ++len;
		for(int i = 0; i < len; ++i) {
			length[num][i] = 1;
		}

		if(len == 4) {
			for(int i = 0; i < 4; ++i) {
				if(figs[num][i] == 2 && figs[num][(i+1)%4] == 2) {
					length[num][i] = 2;
					return;
				}
			}
		}
		else if(len == 5) {
			for(int i = 0; i < 5; ++i) {
				if(figs[num][i] == 2) {
					length[num][i] = length[num][(i+4)%5] = 2;
					return;
				}
			}
		}
		else if(len < 3 || len > 6) {
			System.err.println("Bug : Unknown basic case : " + len);
			System.exit(1);
		}
	}


	protected String[] stringResult(int smooth_num) {
		String[] strs = new String[smooth_num+1];
		int start = 0;
		for(int n = smooth_num; n >= 0; --n) {
			int t = rewrite_part(n);
			if(t < 0) start = 0;
			else if(t == 0) {
				if(figs[n][0] + figs[n][1] == 0) start = (start + figs[n][0] - figs[n][2] + 6) % 6;
				else start = (start - figs[n][1] + 6) % 6;
			}
			int dir = start;
			strs[n] = start + " ";
			for(int i = 1; figs[n][i] != 0; ++i) {
				dir = (dir + figs[n][i] + 6) % 6;
				strs[n] += dir + " ";
			}
			strs[n] += ":";
			for(int i = 0; figs[n][i] != 0; ++i) {
				strs[n] += " " + length[n][i];
			}
		}
		return strs;
	}

	protected AngleDrawingElement[] drawingResult(int smooth_num) {
		AngleDrawingElement[] drawings = new AngleDrawingElement[smooth_num+1];
		int start = 0;
		for(int n = smooth_num; n >= 0; --n) {
			int t = rewrite_part(n);
			if(t < 0) start = 0;
			else if(t == 0) {
				if(figs[n][0] + figs[n][1] == 0) start = (start + figs[n][0] - figs[n][2] + 6) % 6;
				else start = (start - figs[n][1] + 6) % 6;
			}
			int dir = start;
			String drawing = start + " ";
			for(int i = 1; figs[n][i] != 0; ++i) {
				dir = (dir + figs[n][i] + 6) % 6;
				drawing += dir + " ";
			}
			drawing += ":";
			for(int i = 0; figs[n][i] != 0; ++i) {
				drawing += " " + length[n][i];
			}
			drawings[n] = new AngleDrawingElement(drawing, AngleDrawingElement.toElement(figs[n]));
		}
		return drawings;
	}


	protected boolean unit_impossible(int num, int p) {
		// can't we add a convex region as unit length?
		return true;
	}


	protected void rebuild_fig(int num, int p) {
		int len = 0;
		while(figs[num-1][len] != 0) ++len;

		for(int i = 0; i < p; ++i) {
			length[num-1][i] = length[num][i];
		}
		// when smoothed to epsilon
		if(figs[num-1][p] + figs[num-1][p+1] == 0) {
			for(int i = p+2; figs[num-1][i] != 0; ++i) {
				length[num-1][i] = length[num][i-2];
			}
			length[num-1][p] = 1;
			length[num-1][p+1] = 1;
			if(figs[num-1][p] == figs[num-1][p+2]) {
				length[num-1][(p+len-1)%len] -= 1;
				length[num-1][p+2] -= 1;
			}
			else if(figs[num-1][p+2] == -1) {
				length[num-1][p+2] -=1;
			}
			else if(figs[num-1][p+2] == -2) {
				length[num-1][(p+len-1)%len] -= 2;
				length[num-1][p+2] -= 1;
			}
			else if(figs[num-1][p] + figs[num-1][p+2] != 0) {
				length[num-1][(p+len-1)%len] -= 1;
				length[num-1][p+2] += 1;
			}
			else if(figs[num-1][p+2] == 1) {
				length[num-1][(p+len-1)%len] -= 2;
				length[num-1][p+2] += 1;
			}
			else if(figs[num-1][p+2] == 2) {
				length[num-1][(p+len-1)%len] -= 1;
				length[num-1][p+2] += 1;
				length[num-1][p+1] = 2;
			}
			else {
				System.err.println("Bug : Par rebuild : Unknown triple : " + figs[num-1][p] + " " + figs[num-1][p+1] + " " + figs[num-1][p+2]);
				System.exit(1);
			}
		}
		else {
			for(int i = p+1; figs[num-1][i] != 0; ++i) {
				length[num-1][i] = length[num][i-1];
			}
			length[num-1][p] = 1;
			// when -BA -> B
			if(figs[num-1][p] == -1) {
				length[num-1][(p+len-1)%len] -= 1;
				length[num-1][p+1] += 1;
			}
			// when -AB -> -B
			else if(figs[num-1][p] == -2) {
				if(figs[num-1][p+2] < 0) {
					length[num-1][p+2] -= 1;
				}
				else {
					length[num-1][p+2] += 1;
				}
				// trapezoid-case
				if(figs[num-1][p+2] == -2 || figs[num-1][p+2] == 1) {
					length[num-1][p+1] -= 1;
				}
			}
			else {
				System.err.println("Bug : Non-par rebuild : Unknown pair : " + figs[num-1][p] + " " + figs[num-1][p+1]);
				System.exit(1);
			}
		}
	}


	public AngleDrawingElement[] drawing() {
		for(int i = 0; i < MAX_LEN; ++i) {
			for(int j = 0; j < MAX_LEN; ++j) {
				figs[i][j] = 0;
				length[i][j] = 0;
			}
		}
		for(int i = 0; i < fig.length; ++i) {
			figs[0][i] = fig[i];
		}

		int smooth_num = 0;
		int position;
		while((position = rewrite_part(smooth_num)) >= 0) {
			smoothing(smooth_num++, position);
			System.err.println(smooth_num + " rewriting part: " + position);
		}

		basic_fig(smooth_num);

		for(int i = smooth_num; i > 0; --i) {
			int at = rewrite_part(i-1);
			int magn = 2;
			// par case -- need more than 2
			if(figs[i-1][at] + figs[i-1][at+1] == 0) {
				magn = 3;
			}
			if(unit_impossible(i, at)) {
				for(int j = smooth_num; j >= i; --j) {
					for(int k = 0; figs[j][k] != 0; ++k) {
						length[j][k] *= magn;
					}
				}
			}
			rebuild_fig(i, at);
		}
		return drawingResult(smooth_num);
	}

}
