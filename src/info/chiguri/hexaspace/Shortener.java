package info.chiguri.hexaspace;

import java.util.LinkedList;

public class Shortener {
	public static int nextSwappedPart(HexaExpression exp) {
		// require: exp is not convex
		// TODO support for HexaPoint
		for(int i = 0; i < exp.entities.length; ++i) {
			if(exp.entities[i].angle(exp.nextArrow(i)) * exp.rotateSign < 0) {
				return i;
			}
		}
		throw new RuntimeException("No swapped part in " + exp);
	}

	public static int nextRedundantPart(HexaExpression exp) {
		// require: exp has redundant part (consequent arrows exist)
		// TODO support for HexaPoint
		for(int i = 0; i < exp.entities.length; ++i) {
			if(exp.entities[i].angle(exp.nextArrow(i)) == 0) {
				return i;
			}
		}
		throw new RuntimeException("No redundant part in " + exp);
	}

	public static HexaExpression shortening(HexaExpression exp) {
		HexaEntity[] next = new HexaEntity[exp.entities.length - 1];
		int delIndex = nextSwappedPart(exp);
		for(int i = 0; i < delIndex; ++i) {
			next[i] = exp.entities[i];
		}
		for(int i = delIndex + 1; i < exp.entities.length; ++i) {
			next[i - 1] = exp.entities[i];
		}

		return new HexaExpression(true, next);
	}

	public static HexaDrawing[] shorten(HexaExpression orig) {
		if(orig == null || !orig.closed)
			return null; // is it sure?

		LinkedList<HexaExpression> l = new LinkedList<>();

		l.add(orig);
		{
			HexaExpression exp = orig;
			while(!exp.isConvex() || exp.isRedundant()) {
				HexaExpression next = shortening(exp);
				if(next == null) break;
				exp = next;
				l.add(exp);
			}
		}

		HexaExpression[] exps = l.toArray(new HexaExpression[0]);
		int[][] minlengths = new int[exps.length][];

		for(int i = 0; i < exps.length; ++i) {

		}

		HexaDrawing[] results = new HexaDrawing[exps.length];
		for(int i = exps.length-1; i >= 0; --i) {

			results[i] = new HexaDrawing(exps[i].entities, minlengths[i]); // minlengthsではないのだが
		}
		return results;
	}

}
