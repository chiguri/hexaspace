package info.chiguri.hexaspace;

public interface AddDrawingInterface {
	public void addDrawing(HexaDrawing draw);
	public void setDrawing(HexaDrawing draw);
}
