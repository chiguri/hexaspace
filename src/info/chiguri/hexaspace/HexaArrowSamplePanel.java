package info.chiguri.hexaspace;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class HexaArrowSamplePanel extends JPanel {
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// for test
		int size = 100;
		int margin = 50;
		int strmargin = 40;
		for(HexaArrow a : HexaArrow.values()) {
			a.draw(g, margin+size+size*a.number(), margin, size);
		}
		for(HexaArrow a : HexaArrow.values()) {
			a.draw(g, margin, margin+size+size*a.number(), size);
		}
		for(HexaArrow a : HexaArrow.values()) {
			for(HexaArrow b : HexaArrow.values()) {
				g.setFont(getFont().deriveFont(30.0f));
				g.drawString(""+a.angle(b), margin+size+size*b.number()+strmargin, margin+size+size*a.number()+strmargin);
			}
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setSize(800, 800);
		frame.add(new HexaArrowSamplePanel());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
