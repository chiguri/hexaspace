package info.chiguri.hexaspace;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFrame;

public class RecursiveDrawingGenerator implements DrawingGenerator {
	protected int max;
	protected HexaExpression exp;
	protected int[] lengths;
	protected int[][] field;
	protected int[] dx;
	protected int[] dy;
	protected int init_x;
	protected int init_y;

	private boolean naiveMethod(int num, int x, int y) {
		if(num == lengths.length) {
			return (exp.closed ^ (x != init_x || y != init_y));
		}
		final int dx = this.dx[num];
		final int dy = this.dy[num];

		int i;
		for(i = 1; i < max; ++i) {
			lengths[num] = i;
			x += dx;
			y += dy;
			if(field[x][y] != 0) {
				x -= dx;
				y -= dy;
				break;
			}
			field[x][y] = num+1;
			if(naiveMethod(num+1, x, y)) {
				return true;
			}
		}
		for( ; i > 1; --i) {
			field[x][y] = 0;
			x -= dx;
			y -= dy;
		}
		return false;
	}

	private void init(HexaExpression exp) {
		this.exp = exp;
		max = exp.entities.length;
		lengths = new int[exp.entities.length];
		field = new int[lengths.length*4+2][lengths.length*4+2];
		for(int i = 0; i < field.length; ++i) {
			field[0][i] = field[field.length-1][i] = field[i][0] = field[i][field.length-1] = -1;
		}
		dx = new int[exp.entities.length];
		dy = new int[exp.entities.length];
		for(int i = 0; i < exp.entities.length; ++i) {
			dx[i] = HexaSpacePoint.dx(exp.entities[i]);
			dy[i] = HexaSpacePoint.dy(exp.entities[i]);
		}
		init_x = init_y = field.length/2;
	}

	@Override
	public HexaDrawing exp2draw(HexaExpression exp) {
		if(exp == null) return null;
		// expressions with tangent points are not supported yet

		init(exp);

		if(naiveMethod(0, init_x, init_y)) {
			return new HexaDrawing(exp.entities, lengths);
		}
		return null; // cannot generate: runtime exception?
	}

	public static void slideshow(String filename) {
		final JFrame frame = new JFrame();
		final SingleDrawingPanel panel = new SingleDrawingPanel(null);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		frame.add(panel);
		frame.setVisible(true);

		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));

			while(true) {
				String line = reader.readLine();
				if(line == null) break;
				HexaDrawing draw = (new RecursiveDrawingGenerator()).exp2draw(HexaExpression.parseExpression(line));
				if(draw == null) {
					reader.close();
					throw new RuntimeException(line + " cannot be drawn by this method");
				}
				panel.setDrawing(draw);
				frame.repaint();
				Thread.sleep(2000);
			}
			reader.close();
		}
		catch(IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new RecursiveDrawingGenerator().expfile2drawfile("testcase.txt", "drawersfile.txt");
	}
}
