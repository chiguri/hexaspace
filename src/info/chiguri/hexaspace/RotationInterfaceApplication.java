package info.chiguri.hexaspace;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class RotationInterfaceApplication {
	public static final int MAIN_FRAME_WIDTH = 800;
	public static final int MAIN_FRAME_HEIGHT = 800;
	public static final int INPUT_FRAME_WIDTH = 250;
	//public static final int SET_FRAME_HEIGHT = 800;
	public static final int INPUT_FRAME_HEIGHT = 70;
	public static final int WINDOW_MARGIN = 150;

	public static void main(String[] args) {
		view();
	}

	public static void viewSetFrame(final JFrame mainFrame, final AddDrawingInterface panel, final JList<AngleDrawingElement> drawingList) {
		drawingList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		drawingList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				AngleDrawingElement elem = drawingList.getSelectedValue();
				if(elem != null) {
					try {
						panel.setDrawing(HexaDrawing.parseDrawing(elem.drawing));
					}
					catch(RuntimeException ex) {
						ex.printStackTrace();
						panel.setDrawing(null);
					}
				}
				mainFrame.repaint();
			}
		});
		drawingList.setFont(drawingList.getFont().deriveFont(15.0f));

		JFrame frame = new JFrame();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(INPUT_FRAME_WIDTH, MAIN_FRAME_HEIGHT-INPUT_FRAME_HEIGHT);
		frame.setLocation(MAIN_FRAME_WIDTH+WINDOW_MARGIN, INPUT_FRAME_HEIGHT);

		frame.add(new JScrollPane(drawingList));
		frame.setVisible(true);
	}

	public static void updateList(DefaultListModel<String> model, InputStream input) {
		try {
			model.clear();
			BufferedReader reader = new BufferedReader(new InputStreamReader(input));

			while(true) {
				String line = reader.readLine();
				if(line == null) break;
				model.addElement(line);
			}

			reader.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	public static void updateList(DefaultListModel<AngleDrawingElement> model, AngleDrawingElement[] inputs) {
		model.clear();

		for(AngleDrawingElement elem : inputs) {
			model.addElement(elem);
		}
	}

	public static void setInputWindow(final DefaultListModel<AngleDrawingElement> model) {
		final JTextField angles = new JTextField();
		angles.setHorizontalAlignment(JTextField.LEFT);
		angles.setFont(angles.getFont().deriveFont(20.0f));

		final JButton drawButton = new JButton("Draw!");

		JFrame inputFrame = new JFrame();

		drawButton.addActionListener(
			(e) -> {
				try {
					int[] inputs = AngleDrawingElement.parseElement(angles.getText());
					if(inputs.length <= 0) {
						throw new RuntimeException("Input is empty");
					}
					NaiveSmoothing smoother = new NaiveSmoothing(inputs);
					updateList(model, smoother.drawing());
				} catch(RuntimeException ex) {
					JOptionPane.showMessageDialog(inputFrame, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
		);

		// layout settings
		JPanel inputs = new JPanel();

		GridBagLayout layout = new GridBagLayout();
		inputs.setLayout(layout);

		GridBagConstraints constraints = new GridBagConstraints();

		// constraints for draw button
		constraints.gridx = 0;
		constraints.gridy = 0;
		//gbc.gridheight = 1;
		//gbc.gridwidth = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;

		layout.setConstraints(drawButton, constraints);

		// constraints for text field
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		//constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.fill = GridBagConstraints.BOTH;

		layout.setConstraints(angles, constraints);

		inputs.add(drawButton);
		inputs.add(angles);

		// draw window frame
		inputFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		inputFrame.setSize(INPUT_FRAME_WIDTH, INPUT_FRAME_HEIGHT);
		inputFrame.setLocation(MAIN_FRAME_WIDTH+WINDOW_MARGIN, 0);
		inputFrame.add(inputs);
		inputFrame.setVisible(true);

	}

	public static void view() {
		final JFrame frame = new JFrame();
		final SingleDrawingPanel panel = new SingleDrawingPanel(null);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(MAIN_FRAME_WIDTH, MAIN_FRAME_HEIGHT);
		frame.setLocation(WINDOW_MARGIN, 0);
		frame.add(panel);
		frame.setVisible(true);

		DefaultListModel<AngleDrawingElement> model = new DefaultListModel<>();

		setInputWindow(model);

		JList<AngleDrawingElement> list = new JList<>(model);
		viewSetFrame(frame, panel, list);
	}

}
