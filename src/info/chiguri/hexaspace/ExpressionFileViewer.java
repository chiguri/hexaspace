package info.chiguri.hexaspace;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ExpressionFileViewer {
	public static final int MAIN_FRAME_WIDTH = 800;
	public static final int MAIN_FRAME_HEIGHT = 800;
	public static final int SET_FRAME_WIDTH = 250;
	public static final int SET_FRAME_HEIGHT = 800;
	public static final int WINDOW_MARGIN = 150;

	public static void main(String[] args) {
		view("examples.txt", new MinLengthDrawingGenerator());
	}

	public static void viewSetFrame(final JFrame mainFrame, final AddDrawingInterface panel, final DrawingGenerator generator, final JList<String> drawingList) {
		drawingList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		drawingList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				String str = drawingList.getSelectedValue();
				try {
					HexaExpression exp = HexaExpression.parseExpression(str);
					panel.setDrawing(generator.exp2draw(exp));
				}
				catch(RuntimeException ex) {
					ex.printStackTrace();
					panel.setDrawing(null);
				}
				mainFrame.repaint();
			}
		});

		JFrame frame = new JFrame();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(SET_FRAME_WIDTH, SET_FRAME_HEIGHT);
		frame.setLocation(MAIN_FRAME_WIDTH+WINDOW_MARGIN, 0);

		frame.add(new JScrollPane(drawingList));
		frame.setVisible(true);
	}

	public static void view(String expressionfilename, DrawingGenerator generator) {
		final JFrame frame = new JFrame();
		final SingleDrawingPanel panel = new SingleDrawingPanel(null);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(MAIN_FRAME_WIDTH, MAIN_FRAME_HEIGHT);
		frame.setLocation(WINDOW_MARGIN, 0);
		frame.add(panel);
		frame.setVisible(true);

		DefaultListModel<String> model = new DefaultListModel<String>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(expressionfilename));

			while(true) {
				String line = reader.readLine();
				if(line == null) break;
				model.addElement(line);
			}

			reader.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		JList<String> list = new JList<>(model);
		viewSetFrame(frame, panel, generator, list);
	}

}
