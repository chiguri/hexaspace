package info.chiguri.hexaspace;

public class HexaExpression {
	public final HexaEntity[] entities;
	public final boolean closed;
	public final int rotateSign;

	public HexaExpression(boolean closed, HexaEntity[] entities) {
		this.closed = closed;
		this.entities = entities;

		if(closed) {
			int order = 0;
			for(int i = 0; i < entities.length; ++i) {
				order += entities[i].angle(nextArrow(i));
			}
			if(order != 6 && order != -6) {
				throw new RuntimeException("Cannot draw closed figure without crossing");
			}
			if(order > 0) {
				rotateSign = 1;
			}
			else {
				rotateSign = -1;
			}
		}
		else {
			rotateSign = 0;
		}

		Constraints.checkConstraints(this);
	}

	public boolean isConvex() {
		if(!closed)
			return false;
		if(entities[entities.length - 1].angle(entities[0]) * rotateSign < 0) {
			return false;
		}
		for(int i = 1; i < entities.length; ++i) {
			if(entities[i - 1].angle(entities[i]) * rotateSign < 0) {
				return false;
			}
		}
		return true;
	}

	public boolean isRedundant() {
		for(int i = 1; i < entities.length; ++i) {
			if(entities[i - 1].equals(entities[i]))
				return true;
		}
		return false;
	}

	public HexaArrow nextArrow(int i) {
		if(i < 0 || i >= entities.length) {
			return null;
		}
		int j = i;
		do {
			j = (j + 1) % entities.length;
		} while(!(entities[j] instanceof HexaArrow));

		return (HexaArrow) entities[j];
	}

	@Override
	public String toString() {
		String str = "";
		for(HexaEntity e : entities) {
			str += e;
		}
		if(closed) {
			str = "[" + str + "]";
		}
		else {
			str = "(" + str + ")";
		}
		return str;
	}

	public static HexaExpression parseExpression(String str) {
		String str2 = str.replaceAll("[ \t\r\n]+", "");
		boolean closed;
		if(str2.length() < 2) {
			throw new RuntimeException(str + " is not describing expression");
		}
		if(str2.charAt(0) == '[' && str2.charAt(str2.length() - 1) == ']') {
			closed = true;
		}
		else if(str2.charAt(0) == '(' && str2.charAt(str.length() - 1) == ')') {
			closed = false;
		}
		else {
			throw new RuntimeException(str + " is not describing expression");
		}

		return new HexaExpression(closed, HexaDrawing.parseEntities(str2.substring(1, str2.length() - 1)));
	}
}
