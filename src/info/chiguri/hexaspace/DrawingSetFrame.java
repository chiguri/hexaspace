package info.chiguri.hexaspace;

import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class DrawingSetFrame extends JFrame {
	final JFrame frame;
	final AddDrawingInterface add;
	final JTextField arrows, lengths;

	public DrawingSetFrame(JFrame frame, AddDrawingInterface add) {
		super();
		this.frame = frame;
		this.add = add;
		arrows = new JTextField();
		arrows.setHorizontalAlignment(JTextField.LEFT);
		//arrows.setPreferredSize(new Dimension(350, 30));
		lengths = new JTextField();
		lengths.setHorizontalAlignment(JTextField.LEFT);
		//lengths.setPreferredSize(new Dimension(350, 30));

		JPanel buttons = new JPanel();
		//buttons.setLayout(new GridLayout(3, 3));
		//TODO rotate button and mirrored?
		buttons.setLayout(new GridLayout(2, 3));
		for(HexaArrow arrow : HexaArrow.values()) {
			buttons.add(new ArrowButton(arrow));
		}

		JPanel texts = new JPanel();

		GridBagLayout layout = new GridBagLayout();
		texts.setLayout(layout);

		GridBagConstraints constraints = new GridBagConstraints();

		// constraints for arrow label
		constraints.gridx = 0;
		constraints.gridy = 0;
		//gbc.gridheight = 1;
		//gbc.gridwidth = 1;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;

		JLabel arrlabel = new JLabel("Arrow:");
		layout.setConstraints(arrlabel, constraints);

		// constraints for length label
		constraints.gridy = 1;

		JLabel lenlabel = new JLabel("Lengths:");
		layout.setConstraints(lenlabel, constraints);

		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.weightx = 0.0;
		constraints.weighty = 0.0;

		JButton setButton = new JButton("Set");
		setButton.addActionListener(new DrawSetListener());
		layout.setConstraints(setButton, constraints);

		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;

		layout.setConstraints(arrows, constraints);

		constraints.gridx = 1;
		constraints.gridy = 1;
		constraints.weightx = 1.0;
		constraints.weighty = 0.0;
		constraints.fill = GridBagConstraints.HORIZONTAL;

		layout.setConstraints(lengths, constraints);

		texts.add(arrlabel);
		texts.add(arrows);
		texts.add(lenlabel);
		texts.add(lengths);
		texts.add(setButton);

		this.setLayout(new GridLayout(2,1));
		this.add(texts);
		this.add(buttons);
	}

	public static DrawingSetFrame showFrame(JFrame frame, AddDrawingInterface add) {
		DrawingSetFrame setFrame = new DrawingSetFrame(frame, add);
		setFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		setFrame.setSize(400, 400);
		setFrame.setVisible(true);
		return setFrame;
	}

	public void setDrawing() {
		HexaDrawing data;
		// TODO エラー処理
		HexaEntity[] arrs = HexaDrawing.parseEntities(arrows.getText());
		int[] lens = HexaDrawing.parseLengths(lengths.getText());
		data = new HexaDrawing(arrs, lens);

		add.setDrawing(data);
		frame.repaint();
	}

	class DrawSetListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			setDrawing();
		}
	}

	class ArrowButton extends JButton {
		final HexaArrow arrow;
		ArrowButton(HexaArrow arrow) {
			super();
			this.arrow = arrow;
			addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					arrows.setText(arrows.getText()+" "+arrow.number());
					lengths.setText(lengths.getText()+" 1");
					setDrawing();
				}
			});
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			int size = Math.min(getWidth(), getHeight());
			arrow.draw(g, size/10, size/10, (size*9)/10); // need centering?
		}
	}
}
